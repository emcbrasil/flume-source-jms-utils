package com.emc.flume.source.jms;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.conf.Configurable;
import org.apache.flume.event.SimpleEvent;
import org.apache.flume.source.jms.JMSMessageConverter;
import org.apache.flume.source.jms.JMSSourceConfiguration;

public class SemanticSeparatorAwareJMSMessageConverter implements JMSMessageConverter, Configurable {

	public static final String CONVERTER_QUOTE = "converter.quote";
	public static final String CONVERTER_QUOTE_DEFAULT = "\"";

	public static final String CONVERTER_SEPARATOR = "converter.separator";
	public static final String CONVERTER_SEPARATOR_DEFAULT = "\n";

	private static final Logger log = Logger.getLogger(SemanticSeparatorAwareJMSMessageConverter.class.getName());

	private QuotedDataSplitter splitter;

	private Charset charset;

	public Charset getCharset() {
		return charset;
	}

	public void setCharset(final Charset charset) {
		this.charset = charset;
	}

	public List<Event> convert(final Message message) throws JMSException {
		final List<Event> events = new ArrayList<Event>();
		processMessage(message, events);
		return events;
	}

	private void processMessage(final Message message, final List<Event> events) throws JMSException {
		if (message instanceof BytesMessage) {
			processBytesMessage(message, events);
		} else if (message instanceof TextMessage) {
			processTextMessage(message, events);
		} else {
			log.severe("Receiving wrong type of messages");
			throw new java.lang.UnsupportedOperationException("Cannot handle messages of type " + message.getClass().getName());
		}
	}

	private void processTextMessage(final Message message, final List<Event> events) throws JMSException {
		final TextMessage textMessage = (TextMessage) message;
		final String text = textMessage.getText();
		final byte[] bytes = text.getBytes(charset);
		processBody(message, events, bytes);
	}

	private void processBytesMessage(final Message message, final List<Event> events) throws JMSException {
		final BytesMessage bytesMessage = (BytesMessage) message;
		final long length = bytesMessage.getBodyLength();
		if (length > 0L) {
			if (length > Integer.MAX_VALUE) {
				throw new JMSException("Unable to process message "	+ "of size " + length);
			}
			final byte[] body = new byte[(int) length];
			final int count = bytesMessage.readBytes(body);
			if (count != length) {
				throw new JMSException("Unable to read full message. " + "Read " + count + " of total " + length);
			}
			processBody(message, events, body);
		}
	}

	private void processBody(final Message message, final List<Event> events,
			final byte[] body) throws JMSException {
		final List<byte[]> chunks = splitter.split(body);
		for (final byte[] chunk : chunks) {
			final Event event = new SimpleEvent();
			copyMessagePropertiesToEventHeader(message, event);
			log.fine("Writing message body chunk with " + chunk.length + " bytes");
			event.setBody(chunk);
			events.add(event);
		}
	}

	private void copyMessagePropertiesToEventHeader(final Message message, final Event event) throws JMSException {
		final Map<String, String> headers = event.getHeaders();
		@SuppressWarnings("rawtypes")
		final Enumeration propertyNames = message.getPropertyNames();
		while (propertyNames.hasMoreElements()) {
			final String name = propertyNames.nextElement().toString();
			final String value = message.getStringProperty(name);
			headers.put(name, value);
		}
	}

	public void configure(final Context context) {
		final String charsetName = context.getString(
				JMSSourceConfiguration.CONVERTER_CHARSET,
				JMSSourceConfiguration.CONVERTER_CHARSET_DEFAULT).trim();
		charset = Charset.forName(charsetName);

		final String quoteStr = context.getString(CONVERTER_QUOTE, CONVERTER_QUOTE_DEFAULT);
		final String separatorStr = context.getString(CONVERTER_SEPARATOR, CONVERTER_SEPARATOR_DEFAULT);

		try {
			final byte quote = quoteStr.getBytes(charsetName)[0];
			final byte separator = separatorStr.getBytes(charsetName)[0];
			splitter = new QuotedDataSplitter(quote, separator);
		} catch (final UnsupportedEncodingException e) {
			final String errorMsg = "Unsupported charset: " + charsetName;
			log.severe(errorMsg);
			throw new RuntimeException(errorMsg, e);
		}
	}

}
