package com.emc.flume.source.jms;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class QuotedDataSplitter {

	private final byte QUOTE;

	private final byte SEPARATOR;

	private static final Logger log = Logger.getLogger(QuotedDataSplitter.class.getName());

	public QuotedDataSplitter() {
		this((byte) '"', (byte) '\n');
	}

	public QuotedDataSplitter(final byte quote, final byte separator) {
		QUOTE = quote;
		SEPARATOR = separator;
	}

	public List<byte[]> split(final byte[] in) {
		final List<byte[]> chunks = new ArrayList<byte[]>();
		final List<Byte> buffer = new ArrayList<Byte>();
		final int len = in.length;
		boolean valueMode = false;
		boolean quoteMode = false;
		for (int i = 0; i < len; i++) {
			final byte b = in[i];
			if (!valueMode) {
				if (SEPARATOR == b) {
					chunks.add(toByteArray(buffer));
					buffer.clear();
				} else {
					buffer.add(b);
					if (QUOTE == b) {
						valueMode = true;
					}
				}
			} else if (quoteMode) {
				quoteMode = false;
				buffer.add(QUOTE);
				if (QUOTE == b) {
					buffer.add(b);
				} else {
					valueMode = false;
					if (SEPARATOR == b) {
						chunks.add(toByteArray(buffer));
						buffer.clear();
					} else {
						buffer.add(b);
					}
				}
			} else {
				if (QUOTE == b) {
					quoteMode = true;
				} else {
					buffer.add(b);
				}
			}
		}
		if (quoteMode) {
			quoteMode = false;
			buffer.add(QUOTE);
			valueMode = false;
		}
		if (valueMode) {
			warnIncomplete(buffer);
		}
		if (!buffer.isEmpty()) {
			if (chunks.isEmpty()) {
				// Reuse original array
				// (even thought we've already wasted time and memory allocating a linked list with the contents)
				chunks.add(in);
			} else {
				chunks.add(toByteArray(buffer));
			}
		}
		return chunks;
	}

	protected void warnIncomplete(final List<Byte> buffer) {
		log.warning("Incomplete message (EOl/EOF found when reading value): " +
				bufferString(toByteArray(buffer)));
	}

	public static CharBuffer bufferString(final byte[] buffer) {
		return Charset.defaultCharset().decode(ByteBuffer.wrap(buffer));
	}

	private static byte[] toByteArray(final List<Byte> buffer) {
		final byte[] bytes = new byte[buffer.size()];
		for (int idx = 0; idx < bytes.length; idx++) {
			bytes[idx] = buffer.get(idx);
		}
		return bytes;
	}

}
