package com.emc.flume.source.jms.test;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.jms.JMSException;
import javax.jms.Message;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.source.jms.JMSSourceConfiguration;
import org.junit.Assert;
import org.junit.Test;

import com.emc.flume.source.jms.QuotedDataSplitter;
import com.emc.flume.source.jms.SemanticSeparatorAwareJMSMessageConverter;

public class ConversionTest {

	private static final boolean DEBUG_MSGS = false;

	private void assertSize(final String input, final int expectedSize, final boolean expectWarn) {
		if (DEBUG_MSGS) {
			final StackTraceElement[] trace = new Exception().getStackTrace();
			final String trace2 = trace[2].getMethodName();
			final String method = trace2.equals("invoke0") ? trace[1].getMethodName() : trace2;
			System.out.println("[" + method + "]");	
		}
		final AtomicBoolean warned = new AtomicBoolean();
		final QuotedDataSplitter splitter = new QuotedDataSplitter() {
			@Override
			protected void warnIncomplete(final List<Byte> buffer) {
				if (expectWarn) {
					warned.set(true);
				} else {
					throw new AssertionError("Unexpected warning");
				}
			}
		};
		final List<byte[]> chunks = splitter.split(input.getBytes());
		for (final byte[] chunk : chunks) {
			if (DEBUG_MSGS) {
				System.out.println(QuotedDataSplitter.bufferString(chunk).toString().replace("\n", "\\n"));
			}
		}
		try {
			if (expectWarn) {
				Assert.assertTrue(warned.get());
				if (DEBUG_MSGS) {
					System.out.println("WARN");
				}
			}
		} finally {
			if (DEBUG_MSGS) {
				System.out.println("---");
			}
		}
		Assert.assertEquals(expectedSize, chunks.size());
	}

	private void assertSize(final String input, final int expectedSize) {
		assertSize(input, expectedSize, false);
	}

	@Test
	public void testSplitterZero() {
		assertSize("", 0);
	}

	@Test
	public void testSplitterSingle() {
		assertSize("one", 1);
	}

	@Test
	public void testSplitterMultiple() {
		assertSize("one\ntwo", 2);
	}

	@Test
	public void testSplitterSingleQuoted() {
		assertSize("\"one\"", 1);
	}

	@Test
	public void testSplitterQuoteInQuotedContent() {
		assertSize("\"prefix\"\"suffix\"", 1);
	}

	@Test
	public void testSplitterSeparatorInQuotedContent() {
		assertSize("\"prefix\nsuffix\"", 1);
	}

	@Test
	public void testSplitterSeparatorsInWarnContent() {
		assertSize("\"prefix\"\nsuffix\"", 2, true);
	}

	@Test
	public void testSplitterSingleQuotedWarn() {
		assertSize("\"one", 1, true);
	}

	@Test
	public void testSplitterEmpty() {
		assertSize("\n", 1);
	}

	@Test
	public void testSplitterEmptyDouble() {
		assertSize("\n\n", 2);
	}

	@Test
	public void testSplitterSimpleTriple() {
		assertSize("a\nb\nc", 3);
	}

	@Test
	public void testSplitterTerminatedTriple() {
		assertSize("a\nb\nc\n", 3);
	}

	@Test
	public void testSplitterEbcdic() throws UnsupportedEncodingException {
		final String charsetName = "IBM037";
		final Charset charset = Charset.forName(charsetName);
		final byte quote = "\"".getBytes(charsetName)[0];
		final byte separator = "µ".getBytes(charsetName)[0];
		final QuotedDataSplitter splitter = new QuotedDataSplitter(quote, separator);
		final byte[] bytes = new byte[] {
			(byte) 0xF1, (byte) 0xA0,
			0x7F, (byte) 0xF2, (byte) 0xA0, 0x7F, 0x7F, 0x7F, (byte) 0xA0,
			(byte) 0xF3, 0x7F, 0x7F
		};
		final List<byte[]> chunks = splitter.split(bytes);
		Assert.assertEquals(3, chunks.size());
		Assert.assertEquals("1", charset.decode(ByteBuffer.wrap(chunks.get(0))).toString());
		Assert.assertEquals("\"2µ\"\"\"", charset.decode(ByteBuffer.wrap(chunks.get(1))).toString());
		Assert.assertEquals("3\"\"", charset.decode(ByteBuffer.wrap(chunks.get(2))).toString());
	}

	@Test
	public void testMultipleEbcdicTextLines() throws JMSException {
		final String charsetName = "IBM037";
		final Charset charset = Charset.forName(charsetName);

		final Context context = new Context();
		context.put(JMSSourceConfiguration.CONVERTER_CHARSET, charsetName);
		context.put(SemanticSeparatorAwareJMSMessageConverter.CONVERTER_QUOTE, "'");
		context.put(SemanticSeparatorAwareJMSMessageConverter.CONVERTER_SEPARATOR, "µ");

		final SemanticSeparatorAwareJMSMessageConverter converter = new SemanticSeparatorAwareJMSMessageConverter();
		converter.configure(context);

		final Message msg = new SimpleTextMessage("1µ'2µ'''µ3''");
		final List<Event> events = converter.convert(msg);

		Assert.assertEquals(3, events.size());

		Assert.assertEquals("1", charset.decode(ByteBuffer.wrap(events.get(0).getBody())).toString());
		Assert.assertEquals("'2µ'''", charset.decode(ByteBuffer.wrap(events.get(1).getBody())).toString());
		Assert.assertEquals("3''", charset.decode(ByteBuffer.wrap(events.get(2).getBody())).toString());
	}

}
