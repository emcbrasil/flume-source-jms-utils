package com.emc.flume.source.jms.test;

import java.util.Collections;
import java.util.Enumeration;

import javax.jms.JMSException;

public class SimpleTextMessage extends AbstractTextMessage {

	private final String text;
	
	public SimpleTextMessage(final String text) {
		this.text = text;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Enumeration getPropertyNames() throws JMSException {
		return Collections.enumeration(Collections.emptyList());
	}

	@Override
	public String getText() throws JMSException {
		return text;
	}

}
