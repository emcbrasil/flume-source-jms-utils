FROM openjdk:7-jdk

RUN cd /tmp \
 && curl -O http://archive.apache.org/dist/flume/1.5.2/apache-flume-1.5.2-bin.tar.gz \
 && tar zxf apache-flume-1.5.2-bin.tar.gz -C /opt \
 && rm apache-flume-1.5.2-bin.tar.gz \
 && curl -O http://www.webhostingreviewjam.com/mirror/apache/activemq/5.14.4/apache-activemq-5.14.4-bin.tar.gz \
 && tar zxvf apache-activemq-5.14.4-bin.tar.gz -C /opt \
 && rm apache-activemq-5.14.4-bin.tar.gz \
 && cd -

RUN mkdir -p /opt/apache-flume-1.5.2-bin/plugins.d/source-jms-utils/lib \
 && mkdir -p /opt/apache-flume-1.5.2-bin/plugins.d/source-jms-utils/libext

RUN cd /opt/apache-flume-1.5.2-bin/plugins.d/source-jms-utils/libext \
 && curl -O http://www.datanucleus.org/downloads/maven2/javax/jms/jms/1.1/jms-1.1.jar \
 && curl -O http://central.maven.org/maven2/org/apache/activemq/activemq-client/5.14.4/activemq-client-5.14.4.jar \
 && curl -O http://central.maven.org/maven2/javax/management/j2ee/management-api/1.1-rev-1/management-api-1.1-rev-1.jar \
 && curl -O http://central.maven.org/maven2/org/fusesource/hawtbuf/hawtbuf/1.11/hawtbuf-1.11.jar \
 && cd -

COPY target/source-jms-utils-0.0.1-SNAPSHOT.jar /opt/apache-flume-1.5.2-bin/plugins.d/source-jms-utils/lib/

COPY password /opt/apache-flume-1.5.2-bin/conf/

ENV PATH="/opt/apache-activemq-5.14.4/bin:/opt/apache-flume-1.5.2-bin/bin:${PATH}"

COPY flume.conf /opt/apache-flume-1.5.2-bin/conf/

COPY services.sh /

EXPOSE 44444 8161 61616

CMD /bin/bash /services.sh
