# Dell EMC Flume source-jms-utils plugin

Contains:

* A pure Java splitter of byte arrays that supports quoted values; and
* A custom Flume JMS Message Converter for splitting messages.

## Usage

check the example `flume.conf` file.

## Testing

The source code includes unit tests for the splitter.

Integration testing (after `mvn package`) can be done with ActiveMQ in Docker:

    $ docker build -t flume-source-jms-utils .
    $ docker run -it --rm --name flume -p 44444:44444 -p 8161:8161 -p 61616:61616 flume-source-jms-utils

The testing queue is `foo.bar` and the configured separator is `|`. Then, let's send three messages: `abc`, `de"f|""g"hi` and `jkl`. Note the middle message is partly quoted, includes a separator and contains a quoted quote (by double-ocurring), but is not erroneously split.

	$ curl -u admin:admin -d 'body=abc|de"f|""g"hi|jkl' http://localhost:8161/api/message/foo.bar?type=queue

The output should look like this:

    2017-03-15 16:31:30,507 (SinkRunner-PollingRunner-DefaultSinkProcessor) [INFO - org.apache.flume.sink.LoggerSink.process(LoggerSink.java:70)] Event: { headers:{} body: 61 62 63                                        abc }
    2017-03-15 16:31:30,508 (SinkRunner-PollingRunner-DefaultSinkProcessor) [INFO - org.apache.flume.sink.LoggerSink.process(LoggerSink.java:70)] Event: { headers:{} body: 64 65 22 66 7C 22 22 67 22 68 69                de"f|""g"hi }
    2017-03-15 16:31:30,508 (SinkRunner-PollingRunner-DefaultSinkProcessor) [INFO - org.apache.flume.sink.LoggerSink.process(LoggerSink.java:70)] Event: { headers:{} body: 6A 6B 6C                                        jkl }